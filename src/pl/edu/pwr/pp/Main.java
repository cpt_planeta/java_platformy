package pl.edu.pwr.pp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Witaj nieznajomy! Wprowadz swoje imie i nazwisko: ");
		try (BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in))) {
			String imie_nazwisko = bufferRead.readLine();
			Przepowiednie.Wroz(imie_nazwisko);
		}
		catch(IOException ex){
			System.out.println("Error Dobranoc!");
		}
	}
}
