package pl.edu.pwr.pp;

import java.util.Calendar;
import java.util.Random;

public class Przepowiednie {

	/**
	 * Wszystkie wróżby zostały zaczerpnięte z horoskopu dziennego na dzień 16.04.2016 z portalu magia.onet.pl.
	 */
	
	public static String[] MILOSC = {
			"Staraj się nie zawieść jako kochanek.", 
			"Zatęsknisz za gorącą miłością.", 
			"Unikaj spięć z ukochaną osobą.", 
			"Postępuj spokojnie i delikatnie.", 
			"Możliwe trudne relacje z otoczeniem.",
			"Dobrze ułożą się stosunki z rodziną.", 
			"Bądź milszy dla bliskich.", 
			"Potrzeba Ci serdecznego przyjaciela.", 
			"Znajdziesz się w centrum zainteresowania.", 
			"Bądź bardziej powściągliwy w krytykowaniu innych.", 
			"Zawierz intuicji.",
			"Możliwy jest koniec samotności."};
	
	public static String[] PRACA = {
			"Nie narzucaj innym swojej woli.",
			"Będziesz pożądać mocnych wrażeń.",
			"Plany podróży zaczną się urealniać.", 
			"Znajdź trochę czasu dla siebie.", 
			"Rozsądnie planuj wydatki.",
			"Obawy odnośnie pieniędzy są bezzasadne.", 
			"Zajmij się własnymi sprawami.",
			"Postaraj się bardziej wyluzować.",
			"Pamiętaj, że każda przesada bywa szkodliwa.", 
			"Nie dopuszczaj do napiętych sytuacji.",
			"Twoje wysiłki przyniosą dobre rezultaty.", 
			"Nie podejmuj się trudnych zadań." 
	};
	
	public static String[] ZDROWIE = {
			"Przezwyciężaj lenistwo.", 
			"Odprężenie znajdziesz na łonie rodziny.", 
			"Wskazana ostrożność na drodze.", 
			"Popracuj nad swoją kondycją psychiczną.", 
			"Potrzeba Ci kontaktu z przyrodą.", 
			"Postaraj się o właściwy wypoczynek.", 
			"Panuj nad nerwami.",
			"Daj upust swojej niewykorzystanej energii.",
			"Nie rozpraszaj dobrej energii.",
			"Unikaj ciężkostrawnych potraw.", 
			"Naucz się relaksować.",
			"Wprowadź do swego życia więcej radości." 
	};
		
	static int liczbaDlaMilosci() {
		Calendar cal = Calendar.getInstance();
		int dzien = cal.get(Calendar.DAY_OF_MONTH);

		return dzien % 12;
	}
	
	static int liczbaDlaPracy() {
		Random generator = new Random();
		int liczba = generator.nextInt(12);
		
		return liczba;
	}
	
	static int liczbaDlaZdrowia(String imie_nazwisko) {
		int dlugosc = imie_nazwisko.length();
		
		return dlugosc % 12;
	}
	
	public static String przepowiedniaDlaMilosci(){
		return MILOSC[liczbaDlaMilosci()];
	}
	
	public static String przepowiedniaDlaPracy(){
		return PRACA[liczbaDlaPracy()];
	}

	public static String przepowiedniaDlaZdrowia(String imie_nazwisko){
		return ZDROWIE[liczbaDlaZdrowia(imie_nazwisko)];
	}
	
	public static void Wroz(String imie_nazwisko){
		System.out.println(imie_nazwisko + ", oto Twoja przyszlosc: ");
		System.out.println("MILOSC:  " + przepowiedniaDlaMilosci());
		System.out.println("PRACA:   " + przepowiedniaDlaPracy());
		System.out.println("ZDROWIE: " + przepowiedniaDlaZdrowia(imie_nazwisko));
		System.out.println("Zegnaj!");		
	}
}