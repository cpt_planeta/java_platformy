package pl.edu.pwr.pp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.Arrays;

import org.junit.Test;


public class TestPrzepowiednia {
	@Test
	public void liczbaDlaZdrowiaShouldReturn4() {
		int result = Przepowiednie.liczbaDlaZdrowia("Marian Paździoch");
		assertThat(result, equalTo(4));
	}
	
	@Test
	public void liczbaDlaZdrowiaShouldReturn0() {
		int result = Przepowiednie.liczbaDlaZdrowia("");
		assertThat(result, equalTo(0));
	}
	
	@Test
	public void liczbaDlaMilosciShouldReturnMinimum0() {
		int result = Przepowiednie.liczbaDlaMilosci();
		assertThat(result, greaterThanOrEqualTo(0));
	}
	
	@Test
	public void liczbaDlaMilosciShouldReturnMaximum11() {
		int result = Przepowiednie.liczbaDlaMilosci();
		assertThat(result, lessThanOrEqualTo(11));
	}
	
	@Test
	public void liczbaDlaPracyShouldReturnMinimum0() {
		int result = Przepowiednie.liczbaDlaPracy();
		assertThat(result, greaterThanOrEqualTo(0));
	}
	
	@Test
	public void liczbaDlaPracyShouldReturnMaximum11() {
		int result = Przepowiednie.liczbaDlaPracy();
		assertThat(result, lessThanOrEqualTo(11));
	}
	
	@Test
	public void przepowiedniaDlaZdrowiaShouldReturn4thString() {
		String result = Przepowiednie.przepowiedniaDlaZdrowia("Marian Paździoch");
		assertThat(result, equalTo("Potrzeba Ci kontaktu z przyrodą."));
	}
	
	@Test
	public void przepowiedniaDlaMilosciShouldReturnStringFromArray() {
		String fraza = Przepowiednie.przepowiedniaDlaMilosci();
		boolean result = Arrays.asList(Przepowiednie.MILOSC).contains(fraza);
		assertThat(result, equalTo(true));
	}
	
	@Test
	public void przepowiedniaDlaPracyShouldReturnStringFromArray() {
		String fraza = Przepowiednie.przepowiedniaDlaPracy();
		boolean result = Arrays.asList(Przepowiednie.PRACA).contains(fraza);
		assertThat(result, equalTo(true));
	}
}
